import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';

import { Profile } from '../../../typeorm/entities/profile';
import { User } from '../../../typeorm/entities/user';
import { UpdatedUserDto } from '../../dtos/user.dto';
import {
  CreatePostParams,
  CreateProfileParams,
  CreateUserParams,
} from 'src/utils/types';
import { Repository } from 'typeorm';
import { Post } from '../../../typeorm/entities/posts';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User) private userRepo: Repository<User>,
    @InjectRepository(Profile) private profileRepo: Repository<Profile>,
    @InjectRepository(Post) private postRepo: Repository<Post>,
  ) {}
  fetchUsers() {
    return this.userRepo.find({ relations: ['profile', 'posts'] });
  }

  createUser(userDetails: CreateUserParams) {
    const user = this.userRepo.create({ ...userDetails });
    return this.userRepo.save(user);
  }
  updateUserById(id: number, updatedUser: UpdatedUserDto) {
    return this.userRepo.update({ id: id }, { ...updatedUser });
  }
  deleteUser(id: number) {
    return this.userRepo.delete({ id: id });
  }
  async createProfile(id: number, profileDetails: CreateProfileParams) {
    const user = await this.findUserById(id);
    const profile = this.profileRepo.create({ ...profileDetails });
    const savedProfile = await this.profileRepo.save(profile);
    user.profile = savedProfile;
    return this.userRepo.save(user);
  }
  async createPost(id: number, postDetails: CreatePostParams) {
    const user = await this.findUserById(id);
    const post = this.postRepo.create({ ...postDetails, user });
    return this.postRepo.save(post);
  }
  private findUserById(id: number) {
    const user = this.userRepo.findOne({ where: { id: id } });
    if (!user) {
      throw new HttpException(
        'user not found, profile cannot be created',
        HttpStatus.BAD_REQUEST,
      );
    }
    return user;
  }
}
